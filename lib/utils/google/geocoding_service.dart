import 'dart:convert';

import 'package:cloudwatch/utils/google/place.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class GeocodingService {
  static final String _apiKey = dotenv.get( "GOOGLE_GEOCODING_API_KEY" );

  static Future<List<Place>> getPlaces(String address) async {
    final apiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=$_apiKey';
    final response = await http.get( Uri.parse( apiUrl ) );

    if (response.statusCode == 200) {
      final jsonBody = json.decode( response.body );
      final results = jsonBody['results'] as List<dynamic>;

      List<Place> places = [];

      for (var result in results) {
        final geometry = result['geometry'];
        final location = geometry['location'];

        final place = Place(
          latitude: location['lat'],
          longitude: location['lng'],
          name: result['formatted_address'] ?? '-',
          description: result['formatted_address'] ?? '-',
        );

        places.add(place);
      }

      return places;
    } else {
      throw Exception('Failed to fetch places');
    }
  }

  static Future<Place> getPlaceFromLatLon(double latitude, double longitude) async {
    final apiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=$latitude,$longitude&key=$_apiKey';
    final response = await http.get(Uri.parse(apiUrl));

    if (response.statusCode == 200) {
      final jsonBody = json.decode(response.body);
      final results = jsonBody['results'] as List<dynamic>;

      if (results.isNotEmpty) {
        final result = results.first;
        final name = result['formatted_address'] ?? '';
        final description = result['formatted_address'] ?? '';

        return Place(
          latitude: latitude,
          longitude: longitude,
          name: name,
          description: description,
        );
      }
    }

    throw Exception('Failed to fetch current place');
  }
}