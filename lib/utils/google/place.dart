class Place {
  final double latitude;
  final double longitude;
  final String name;
  final String description;

  Place({
    required this.latitude,
    required this.longitude,
    required this.name,
    required this.description,
  });
}