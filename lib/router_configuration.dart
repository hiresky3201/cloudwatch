import 'package:cloudwatch/client/home/home_page_connector.dart';
import 'package:cloudwatch/client/search/search_page_connector.dart';
import 'package:flutter/material.dart';

class RouterConfiguration{
  static Map<String, Widget Function( BuildContext )> kAppRoutes = {
    '/home': ( context ) => const HomePageConnector(),
    '/search': ( context ) => const SearchPageConnector(),
  };
}