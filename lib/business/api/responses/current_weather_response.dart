import 'package:cloudwatch/business/models/weather.dart';

class CurrentWeatherResponse {
  double lat;
  double lon;
  String timezone;
  Weather weather;

  CurrentWeatherResponse({
    required this.lat,
    required this.lon,
    required this.timezone,
    required this.weather
  });

  factory CurrentWeatherResponse.fromJson(Map<String, dynamic> json){
    Map<String, dynamic> weatherFields = {};

    return CurrentWeatherResponse(
      lat: double.parse( json['lat'].toString() ),
      lon: double.parse( json['lon'].toString() ),
      timezone: json['timezone'],
      weather: Weather.fromJson( json['current'] ),
    );
  }
}