import 'dart:convert';

import 'package:cloudwatch/business/api/exceptions/api_exception.dart';
import 'package:cloudwatch/business/api/responses/current_weather_response.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class WeatherApiClient{
  static final String _apiUrl = dotenv.get( "WEATHER_API_URL" );
  static final String _apiKey = dotenv.get( "WEATHER_API_KEY" );

  static Future<CurrentWeatherResponse> getCurrentWeather( lat, lon ) async {
    Uri path = Uri.parse( '$_apiUrl/data/3.0/onecall?lat=$lat&lon=$lon&units=metric&appid=$_apiKey' );
    var res = await http.get( path ).timeout( const Duration( seconds: 8 ) );

    var jsonBody = json.decode( utf8.decode( res.bodyBytes ) );

    if( res.statusCode != 200 ){
      throw ApiException( jsonBody['message'] );
    }

    return CurrentWeatherResponse.fromJson( jsonBody );
  }
}