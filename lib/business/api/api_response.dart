class ApiResponse{
  dynamic data;

  List<String> errors = [];
  List<String> warnings = [];

  ApiResponse();
  ApiResponse.fromData({ this.data });
  ApiResponse.fromError({
    required this.errors,
    required this.warnings
  });

  factory ApiResponse.fromJson(Map<String, dynamic> parsedJson){
    List<String> errors = [];
    List<String> warnings = [];

    if( parsedJson['errors'] != null ) {
      errors = List<String>.from( parsedJson['errors'] );
    }

    if( parsedJson['warnings'] != null ) {
      warnings = List<String>.from( parsedJson['warnings'] );
    }

    ApiResponse response = ApiResponse.fromData( data: parsedJson['data'] );
    response.warnings = warnings;
    response.errors = errors;

    return response;
  }
}