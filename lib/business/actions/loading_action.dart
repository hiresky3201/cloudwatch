import 'package:async_redux/async_redux.dart';
import 'package:cloudwatch/business/store/app_state.dart';

class LoadingAction extends ReduxAction<AppState>{
  final bool loading;

  LoadingAction({ required this.loading });

  @override
  AppState? reduce() {
    return state.copy( loading: loading );
  }
}