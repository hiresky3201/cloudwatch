import 'dart:async';

import 'package:cloudwatch/business/actions/loading_action.dart';
import 'package:cloudwatch/business/actions/set_place_action.dart';
import 'package:cloudwatch/utils/google/geocoding_service.dart';
import 'package:cloudwatch/utils/google/place.dart';
import 'package:location/location.dart';
import 'package:async_redux/async_redux.dart';
import 'package:cloudwatch/business/store/app_state.dart';
import 'package:permission_handler/permission_handler.dart';

class FetchCurrentPlaceAction extends ReduxAction<AppState>{
  final bool requestPermission;

  FetchCurrentPlaceAction({
    required this.requestPermission
  });

  @override
  Future<AppState?> reduce() async {
    if( !(await Permission.location.isGranted ) ){
      if( !requestPermission ) return null;
      if ( !(await Permission.location.request().isGranted ) ) return null;
    }

    Location location = Location();
    bool serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.serviceEnabled();
      if( !serviceEnabled ) return null;
    }

    var locationData = await location.getLocation();
    Place place = await GeocodingService.getPlaceFromLatLon( locationData.latitude ?? 0 , locationData.longitude ?? 0 );

    dispatch( SetPlaceAction( place: place ) );
    return null;
  }

  @override
  FutureOr<void> before() => dispatch( LoadingAction( loading: true ) );

  @override
  void after() => dispatch( LoadingAction( loading: false ) );
}