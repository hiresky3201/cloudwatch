import 'package:async_redux/async_redux.dart';
import 'package:cloudwatch/business/actions/fetch_weather_action.dart';
import 'package:cloudwatch/business/models/lat_lon.dart';
import 'package:cloudwatch/business/store/app_state.dart';
import 'package:cloudwatch/utils/google/place.dart';

class SetPlaceAction extends ReduxAction<AppState>{
  final Place place;

  SetPlaceAction({ required this.place });

  @override
  AppState? reduce() {
    return state.copy(
      currentPlace: place
    );
  }

  @override
  void after() {
    super.after();
    dispatch( FetchWeatherAction( latLon: LatLon( place.latitude, place.longitude ) ) );
  }
}