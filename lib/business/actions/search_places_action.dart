import 'dart:async';

import 'package:async_redux/async_redux.dart';
import 'package:cloudwatch/business/actions/loading_action.dart';
import 'package:cloudwatch/business/store/app_state.dart';
import 'package:cloudwatch/utils/google/geocoding_service.dart';
import 'package:cloudwatch/utils/google/place.dart';

class SearchPlacesAction extends ReduxAction<AppState>{
  final String query;
  SearchPlacesAction({
    required this.query
  });

  @override
  Future<AppState?> reduce() async {
    if( query.isEmpty ) return state.copy( placeSearchResults: [] );

    List<Place> places = await GeocodingService.getPlaces( query );
    return state.copy(
      placeSearchResults: places
    );
  }

  @override
  FutureOr<void> before() => dispatch( LoadingAction( loading: true ) );

  @override
  void after() => dispatch( LoadingAction( loading: false ) );
}