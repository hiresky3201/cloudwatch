import 'dart:async';

import 'package:async_redux/async_redux.dart';
import 'package:cloudwatch/business/actions/loading_action.dart';
import 'package:cloudwatch/business/api/responses/current_weather_response.dart';
import 'package:cloudwatch/business/api/weather_api_client.dart';
import 'package:cloudwatch/business/models/lat_lon.dart';
import 'package:cloudwatch/business/store/app_state.dart';

class FetchWeatherAction extends ReduxAction<AppState>{
  final LatLon latLon;
  FetchWeatherAction({ required this.latLon });

  @override
  Future<AppState?> reduce() async {
    CurrentWeatherResponse response = await WeatherApiClient.getCurrentWeather( latLon.lat, latLon.lon );

    return state.copy(
      currentWeather: response.weather
    );
  }

  @override
  FutureOr<void> before() => dispatch( LoadingAction( loading: true ) );

  @override
  void after() => dispatch( LoadingAction( loading: false ) );
}