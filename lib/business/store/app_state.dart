import 'package:cloudwatch/business/models/weather.dart';
import 'package:cloudwatch/utils/google/place.dart';
import 'package:flutter/foundation.dart';

@immutable
class AppState{
  final bool loading;
  final Place? currentPlace;
  final Weather? currentWeather;
  final List<Place> placeSearchResults;

  const AppState({
    this.currentPlace,
    this.currentWeather,
    required this.placeSearchResults,
    required this.loading
  });

  AppState copy({ Place? currentPlace, Weather? currentWeather, bool? loading, List<Place>? placeSearchResults }){
    return AppState(
      loading: loading ?? this.loading,
      currentPlace: currentPlace ?? this.currentPlace,
      currentWeather: currentWeather ?? this.currentWeather,
      placeSearchResults: placeSearchResults ?? this.placeSearchResults,
    );
  }

  static Future<AppState> initialState() async {
    return const AppState(
      loading: false,
      placeSearchResults: []
    );
  }

  @override
  int get hashCode => Object.hashAll([ currentPlace, currentWeather, loading, placeSearchResults ]);

  @override
  bool operator ==(Object other) {
    if( identical( this , other ) ) return true;
    if( other is! AppState || runtimeType != other.runtimeType ) return false;
    return currentWeather == other.currentWeather && currentPlace == other.currentPlace && listEquals( placeSearchResults, other.placeSearchResults );
  }
}