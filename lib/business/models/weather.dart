import 'package:cloudwatch/business/models/weather_status.dart';

class Weather{
  DateTime sunrise;
  DateTime sunset;
  double temp;
  double feelsLike;
  double pressure;
  double humidity;
  double dewPoint;
  double uvi;
  double cloudiness;
  double visibility;
  double windSpeed;
  double windDeg;
  double rain;
  List<WeatherStatus> statuses;

  Weather({
    required this.sunrise,
    required this.sunset,
    required this.temp,
    required this.feelsLike,
    required this.pressure,
    required this.humidity,
    required this.dewPoint,
    required this.uvi,
    required this.cloudiness,
    required this.visibility,
    required this.windSpeed,
    required this.windDeg,
    required this.rain,
    required this.statuses
  });

  factory Weather.fromJson(Map<String, dynamic> json){
    return Weather(
      sunrise: DateTime.fromMillisecondsSinceEpoch( json['sunrise'], isUtc: true ),
      sunset: DateTime.fromMillisecondsSinceEpoch( json['sunset'], isUtc: true ),
      temp: double.parse(json['temp'].toString()),
      feelsLike: double.parse(json['feels_like'].toString()),
      pressure: double.parse(json['pressure'].toString()),
      humidity: double.parse(json['humidity'].toString()),
      dewPoint: double.parse(json['dew_point'].toString()),
      uvi: double.parse(json['uvi'].toString()),
      cloudiness: double.parse(json['clouds'].toString()),
      visibility: double.parse(json['visibility'].toString()),
      windSpeed: double.parse(json['wind_speed'].toString()),
      windDeg: double.parse(json['wind_deg'].toString()),
      rain: double.parse(json['rain'] ?? "0"),
      statuses: List<WeatherStatus>.from(
        json['weather'].map( ( w ) => WeatherStatus.fromJson( w ) )
      )
    );
  }
}