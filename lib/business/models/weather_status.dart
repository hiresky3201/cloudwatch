class WeatherStatus{
  String status;
  String icon;
  String description;

  WeatherStatus({
    required this.status,
    required this.icon,
    required this.description
  });

  factory WeatherStatus.fromJson(Map<String, dynamic> json){
    return WeatherStatus(
        status: json['main'],
        icon: '${json['icon']}@2x.png',
        description: json['description']
    );
  }
}