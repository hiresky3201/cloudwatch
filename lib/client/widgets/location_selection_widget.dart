import 'package:cloudwatch/business/models/lat_lon.dart';
import 'package:cloudwatch/utils/google/place.dart';
import 'package:flutter/material.dart';

class LocationSelectionWidget extends StatefulWidget{
  final List<Place> places;
  final Function( LatLon latLon ) fetchWeatherCallback;
  final Function( String query ) searchPlacesCallback;

  const LocationSelectionWidget({
    super.key,
    required this.places,
    required this.searchPlacesCallback,
    required this.fetchWeatherCallback,
  });

  @override
  State<StatefulWidget> createState() => _LocationSelectionWidgetState();
}

class _LocationSelectionWidgetState extends State<LocationSelectionWidget>{
  String place = '';
  Place? selectedPlace;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextField(
          onChanged: (value) {
            setState(() {
              place = value;
            });
          },
          decoration: const InputDecoration(
            labelText: 'Enter a place',
          ),
        ),
        ElevatedButton(
          onPressed: () => widget.searchPlacesCallback( place ),
          child: const Text('Get Places'),
        ),
        const SizedBox(height: 16.0),
        DropdownButton<Place>(
          value: selectedPlace,
          hint: const Text('Select a place'),
          onChanged: (newValue) {
            setState(() {
              selectedPlace = newValue;
            });
          },
          items: widget.places.map<DropdownMenuItem<Place>>(
                (place) {
              return DropdownMenuItem<Place>(
                value: place,
                child: Text(place.name),
              );
            },
          ).toList(),
        ),
        const SizedBox(height: 16.0),
        if (selectedPlace != null)
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Selected Place:'),
              Text('Name: ${selectedPlace!.name}'),
              Text('Description: ${selectedPlace!.description}'),
              Text('Latitude: ${selectedPlace!.latitude}'),
              Text('Longitude: ${selectedPlace!.longitude}'),
            ],
          ),
      ],
    );
  }
}