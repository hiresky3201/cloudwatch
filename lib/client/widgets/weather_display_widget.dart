import 'package:cloudwatch/business/models/lat_lon.dart';
import 'package:cloudwatch/business/models/weather.dart';
import 'package:cloudwatch/utils/google/place.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class WeatherDisplayWidget extends StatelessWidget{
  static final String? _weatherIconImagesUrl = dotenv.get('WEATHER_ICONS_URL');
  final bool loading;
  final Place? currentPlace;
  final Weather? weather;
  final Function( LatLon latLon ) fetchWeatherCallback;

  const WeatherDisplayWidget({
    super.key,
    required this.loading,
    required this.weather,
    required this.currentPlace,
    required this.fetchWeatherCallback
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _inferWeatherDisplay( context )
      ],
    );
  }
  Widget _inferWeatherDisplay( BuildContext context ){
    if( loading ){
      return const Padding(
        padding: EdgeInsets.all( 20 ),
        child: CircularProgressIndicator(),
      );
    }
    else if( weather == null ){
      return const Padding(
        padding: EdgeInsets.all( 10 ),
        child: Text('..'),
      );
    }

    return Padding(
      padding: const EdgeInsets.only( top: 20 ),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular( 10 ),
            border: Border.all(
              color: Colors.white,
              width: 2,
            )
        ),
        child: Padding(
          padding: const EdgeInsets.only( bottom: 5, left: 5, right: 5 ),
          child: Column(
            children: [
              _buildWeatherStatuses( context ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildWeatherStatuses( BuildContext context ){
    String combinedStatuses = List<String>.of( weather!.statuses.map( ( status ) => status.status ) ).join(',');
    String combinedDescriptions = List<String>.of( weather!.statuses.map( ( status ) => status.description ) ).join(',');

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ...List<Widget>.of(
                weather!.statuses.map( ( status ) => Image.network( '$_weatherIconImagesUrl/${weather!.statuses[0].icon}' ) )
            ),
            Text( '${weather!.temp.toStringAsFixed(0)}°', style: TextStyle( color: Colors.white, fontSize: 35 ), ),
          ],
        ),
        Text( currentPlace!.name, style: const TextStyle( color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold, ), textAlign: TextAlign.center, ),
        Text( combinedStatuses, style: const TextStyle( color: Colors.white, fontSize: 20 ), ),
        Text( combinedDescriptions, style: const TextStyle( color: Colors.white ), ),
      ],
    );
  }
}