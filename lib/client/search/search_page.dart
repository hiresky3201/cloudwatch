import 'package:cloudwatch/utils/google/place.dart';
import 'package:flutter/material.dart';
import 'package:async/async.dart';

class SearchPage extends StatefulWidget{
  final Place? currentPlace;
  final bool loading;
  final List<Place> placeSearchResult;
  final Function (Place place) setPlaceCallback;
  final Function(bool requestPermission) fetchCurrentPlaceCallback;
  final Function(String query) searchPlacesCallback;

  const SearchPage({
    super.key,
    required this.loading,
    required this.currentPlace,
    required this.setPlaceCallback,
    required this.placeSearchResult,
    required this.searchPlacesCallback,
    required this.fetchCurrentPlaceCallback,
  });

  @override
  State<StatefulWidget> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage>{
  // -- Required to debounce search to prevent multiple calls within ms
  CancelableOperation<String>? _debounceOperation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar( context ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: _buildBody( context ),
      ),
    );
  }

  Widget _buildBody(BuildContext context){
    if( widget.loading ){
      return const Padding(
        padding: EdgeInsets.only( top: 25, bottom: 25 ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(),
          ],
        ),
      );
    }

    return Column(
      children: [
        _inferCurrentPlaceSelection( context ),
        _buildSearchResults( context )
      ],
    );
  }

  Widget _inferCurrentPlaceSelection(BuildContext context){
    if( widget.currentPlace != null ){
      return MaterialButton(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text( widget.currentPlace!.name ),
            const Row(
              children: [
                Icon( Icons.location_on, size: 18, ),
                Text('Your location', style: TextStyle( fontSize: 12 ),)
              ],
            )
          ],
        ),
        onPressed: () => widget.setPlaceCallback( widget.currentPlace! ),
      );
    }

    return MaterialButton(
      child: const Text('Fetch current location'),
      onPressed: () => widget.fetchCurrentPlaceCallback( true ),
    );
  }

  Widget _buildSearchResults(BuildContext context){
    Widget inferredSearchResultsWidget = Container();

    if( widget.placeSearchResult.isEmpty ) {
      inferredSearchResultsWidget = const Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(Icons.search),
            Text('No places found')
          ],
        ),
      );
    }
    else {
      inferredSearchResultsWidget = Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widget.placeSearchResult.map( ( p ) {
          return Row(
            children: [
              Expanded(
                child: MaterialButton(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text( p.name ),
                  ),
                  onPressed: () => widget.setPlaceCallback( p ),
                ),
              )
            ],
          );
        }).toList().cast<Widget>(),
      );
    }

    return Padding(
      padding: const EdgeInsets.only(
        left: 15,
        right: 15,
        top: 20
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text('Search results'),
          const Divider(),
          inferredSearchResultsWidget
        ],
      ),
    );
  }

  PreferredSizeWidget _buildAppBar(BuildContext context){
    return AppBar(
      elevation: 0.0,
      bottom: PreferredSize(
        preferredSize: const Size.fromHeight(1.0), // Set preferredSize height to desired border thickness
        child: Container(
          color: Colors.grey, // Border color
          height: 1.0, // Border thickness
        ),
      ),
      automaticallyImplyLeading: true,
      leadingWidth: 20,
      title: TextField(
        decoration: const InputDecoration(
          hintText: 'Search for a place',
          border: OutlineInputBorder(
            borderSide: BorderSide.none
          )
        ),
        onChanged: handleSearchInputChange,
      ),
    );
  }

  void handleSearchInputChange(String value) {
    _debounceOperation?.cancel();
    _debounceOperation = CancelableOperation<String>.fromFuture(
      Future<String>.delayed( const Duration( milliseconds: 500 ) , () => value),
    );

    _debounceOperation!.value.then( widget.searchPlacesCallback );
  }

  @override
  void dispose() {
    _debounceOperation?.cancel();
    super.dispose();
  }
}