import 'package:async_redux/async_redux.dart';
import 'package:cloudwatch/business/actions/fetch_current_place_action.dart';
import 'package:cloudwatch/business/actions/search_places_action.dart';
import 'package:cloudwatch/business/actions/set_place_action.dart';
import 'package:cloudwatch/business/store/app_state.dart';
import 'package:cloudwatch/client/search/search_page.dart';
import 'package:cloudwatch/utils/google/place.dart';
import 'package:flutter/material.dart';

class SearchPageConnector extends StatelessWidget{
  const SearchPageConnector({super.key});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, SearchPageViewModel>(
      vm: () => SearchPageViewModelFactory(),
      onInit: ( store ) {
        store.dispatch( FetchCurrentPlaceAction( requestPermission: false ) );
      },
      builder: ( BuildContext context, SearchPageViewModel vm ) => SearchPage(
        loading: vm.loading,
        currentPlace: vm.currentPlace,
        setPlaceCallback: vm.setPlaceCallback,
        fetchCurrentPlaceCallback: vm.fetchCurrentPlaceCallback,
        searchPlacesCallback: vm.searchPlacesCallback,
        placeSearchResult: vm.placeSearchResult,
      )
    );
  }
}

class SearchPageViewModel extends Vm{
  final bool loading;
  final List<Place> placeSearchResult;
  final Place? currentPlace;
  final Function (Place place) setPlaceCallback;
  final Function(bool requestPermission) fetchCurrentPlaceCallback;
  final Function(String query) searchPlacesCallback;

  SearchPageViewModel({
    required this.loading,
    required this.currentPlace,
    required this.setPlaceCallback,
    required this.placeSearchResult,
    required this.fetchCurrentPlaceCallback,
    required this.searchPlacesCallback,
  }) : super( equals: [ currentPlace, placeSearchResult, loading ] );
}

class SearchPageViewModelFactory extends VmFactory<AppState, SearchPageConnector, SearchPageViewModel>{
  @override
  SearchPageViewModel? fromStore() {
    return SearchPageViewModel(
      loading: state.loading,
      currentPlace: state.currentPlace,
      placeSearchResult: state.placeSearchResults,
      setPlaceCallback: ( place ){
        dispatch( SetPlaceAction(place: place) );
        dispatch( NavigateAction.pushNamedAndRemoveAll( '/home' ) );
      },
      fetchCurrentPlaceCallback: ( requestPermission ) => dispatch( FetchCurrentPlaceAction(requestPermission: requestPermission) ),
      searchPlacesCallback: ( query ) => dispatch( SearchPlacesAction(query: query) )
    );
  }
}