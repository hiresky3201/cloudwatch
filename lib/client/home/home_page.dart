import 'package:cloudwatch/business/models/lat_lon.dart';
import 'package:cloudwatch/business/models/weather.dart';
import 'package:cloudwatch/utils/google/place.dart';
import 'package:flutter/material.dart';
import 'package:cloudwatch/client/widgets/weather_display_widget.dart';

class HomePage extends StatelessWidget{
  final bool loading;
  final Place? currentPlace;
  final Weather? currentWeather;
  final Function navigateToSearchCallback;
  final Function( LatLon latLon ) fetchWeatherCallback;

  const HomePage({
    super.key,
    required this.loading,
    required this.currentPlace,
    required this.currentWeather,
    required this.fetchWeatherCallback,
    required this.navigateToSearchCallback,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: const [
                  0, 0.3, 0.6, 0.96
                ],
                colors: [
                  Colors.cyanAccent.shade100,
                  Colors.lightBlueAccent,
                  Colors.indigoAccent.shade200,
                  Colors.purpleAccent.shade100
                ],
              )
          ),
          child: _buildHomeBody( context ),
        )
      ),
    );
  }

  Widget _buildHomeBody( BuildContext context ){
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset('assets/img/logo.png'),
            _buildWeatherSection( context )
          ],
        ),
      ),
    );
  }

  Widget _buildWeatherSection( BuildContext context ){
    return Padding(
      padding: const EdgeInsets.all( 15 ),
      child: Column(
        children: [
          TextButton(
            style: OutlinedButton.styleFrom(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              shape: const RoundedRectangleBorder(
                side: BorderSide(
                  color: Colors.white,
                  width: 2.0
                ),
                borderRadius: BorderRadius.all(Radius.circular(2)),
              ),
            ),
            child: const Text('Select a place', style: TextStyle( color: Colors.white ),),
            onPressed: () => navigateToSearchCallback()
          ),
          WeatherDisplayWidget(
            loading: loading,
            currentPlace: currentPlace,
            weather: currentWeather,
            fetchWeatherCallback: fetchWeatherCallback,
          )
        ],
      ),
    );
  }
}