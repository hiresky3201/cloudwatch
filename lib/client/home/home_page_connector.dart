import 'package:async_redux/async_redux.dart';
import 'package:cloudwatch/business/actions/fetch_weather_action.dart';
import 'package:cloudwatch/business/models/lat_lon.dart';
import 'package:cloudwatch/business/models/weather.dart';
import 'package:cloudwatch/business/store/app_state.dart';
import 'package:cloudwatch/client/home/home_page.dart';
import 'package:cloudwatch/utils/google/place.dart';
import 'package:flutter/material.dart';

class HomePageConnector extends StatelessWidget{
  const HomePageConnector({super.key});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, HomePageViewModel>(
      vm: () => HomePageViewModelFactory(),
      builder: ( context, vm ) => HomePage(
        loading: vm.loading,
        currentPlace: vm.currentPlace,
        currentWeather: vm.currentWeather,
        fetchWeatherCallback: vm.fetchWeatherCallback,
        navigateToSearchCallback: vm.navigateToSearchCallback,
      ),
    );
  }
}

class HomePageViewModel extends Vm{
  final bool loading;
  final Place? currentPlace;
  final Weather? currentWeather;
  final Function navigateToSearchCallback;
  final Function(LatLon latLon) fetchWeatherCallback;

  HomePageViewModel({
    required this.loading,
    required this.currentPlace,
    required this.currentWeather,
    required this.navigateToSearchCallback,
    required this.fetchWeatherCallback,
  }) : super( equals: [ loading, currentWeather, currentPlace ] );
}

class HomePageViewModelFactory extends VmFactory<AppState, HomePageConnector, HomePageViewModel>{
  @override
  HomePageViewModel? fromStore() => HomePageViewModel(
    loading: state.loading,
    currentPlace: state.currentPlace,
    currentWeather: state.currentWeather,
    navigateToSearchCallback: () => dispatch( NavigateAction.pushNamed('/search') ),
    fetchWeatherCallback: (latLon) => dispatch( FetchWeatherAction( latLon: latLon) )
  );
}