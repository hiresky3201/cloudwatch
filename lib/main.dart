import 'package:async_redux/async_redux.dart';
import 'package:cloudwatch/business/store/app_state.dart';
import 'package:cloudwatch/client/home/home_page_connector.dart';
import 'package:cloudwatch/router_configuration.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter/material.dart';

// -- Allows global use of store if needed
Store<AppState>? store;
final navigatorKey = GlobalKey<NavigatorState>();

void main() async {
  // -- Ensures that the flutter framework is initialized before proceeding
  WidgetsFlutterBinding.ensureInitialized();

  // -- App setup
  await dotenv.load( fileName: '.env' );

  // -- Allows store to dispatch navigation actions
  NavigateAction.setNavigatorKey( navigatorKey );
  Store<AppState> initialStore = Store<AppState>(
    initialState: await AppState.initialState(),
  );

  store = initialStore;
  runApp( MyApp( store: initialStore ) );
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  const MyApp({
    super.key,
    required this.store
  });

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
        store: store,
        child: MaterialApp(
          title: 'Cloudwatch',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
            useMaterial3: true,
          ),
          initialRoute: '/home',
          navigatorKey: navigatorKey,
          routes: RouterConfiguration.kAppRoutes,
          home: const HomePageConnector(),
        )
    );
  }
}